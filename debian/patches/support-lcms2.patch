Description: Patch to migrate to lcms2
Author: Tobias Frost <tobi@debian.org>
Bug-Debian: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=745516
Forwarded: no
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/profilemanager/lcmswrapper.h
+++ b/profilemanager/lcmswrapper.h
@@ -19,7 +19,7 @@
 #endif
 #endif
 
-#include <lcms.h>
+#include <lcms2.h>
 #include "md5.h"
 #include "imagesource_types.h"
 
@@ -111,7 +111,7 @@
 	public:
 	CMSWhitePoint(int degk)
 	{
-		cmsWhitePointFromTemp(degk,&whitepoint);
+		cmsWhitePointFromTemp(&whitepoint,degk);
 	}
 	protected:
 	cmsCIExyY whitepoint;
@@ -147,18 +147,18 @@
 	public:
 	CMSGamma(float gamma)
 	{
-		gammatable=cmsBuildGamma(256,gamma);
+		gammatable=cmsBuildGamma(NULL,gamma);
 	}
 	~CMSGamma()
 	{
-		cmsFreeGamma(gammatable);
+		cmsFreeToneCurve(gammatable);
 	}
-	LPGAMMATABLE GetGammaTable()
+	cmsToneCurve* GetGammaTable()
 	{
 		return(gammatable);
 	}
 	protected:
-	LPGAMMATABLE gammatable;
+	cmsToneCurve* gammatable;
 	friend class CMSProfile;
 	friend class CMSRGBGamma;
 };
@@ -182,7 +182,7 @@
 	}
 	protected:
 	CMSGamma redgamma,greengamma,bluegamma;
-	LPGAMMATABLE gammatables[3];
+	cmsToneCurve* gammatables[3];
 	friend class CMSProfile;
 };
 
--- a/configure.ac
+++ b/configure.ac
@@ -30,7 +30,7 @@
 PKG_CHECK_MODULES([GTK2], [gtk+-2.0])
 PKG_CHECK_MODULES([GDKPIXBUF2], [gdk-pixbuf-2.0])
 PKG_CHECK_MODULES([GP], [gutenprint])
-PKG_CHECK_MODULES([LCMS], [lcms])
+PKG_CHECK_MODULES([LCMS2], [lcms2])
 AC_CHECK_LIB(m,pow, LIBM_LIBS="-lm")
 AC_SUBST([LIBM_LIBS])
 AC_CHECK_LIB([pnm], [pnm_readpaminit],,,[-lm])
--- a/profilemanager/lcmswrapper.cpp
+++ b/profilemanager/lcmswrapper.cpp
@@ -18,6 +18,8 @@
 #include <iostream>
 #include <fstream>
 
+#include <string.h>
+
 #include "../support/debug.h"
 
 #ifdef HAVE_CONFIG_H
@@ -33,6 +35,51 @@
 
 using namespace std;
 
+// This snippets has been stolen from here:
+// https://www.vuiis.vanderbilt.edu/~welcheb/mac%20installs/GIMP/separate+-0.5.8/lcms_wrapper.h
+typedef cmsICCHeader icHeader;
+
+typedef enum {
+  icSigInputClass              = cmsSigInputClass,
+  icSigDisplayClass            = cmsSigDisplayClass,
+  icSigOutputClass             = cmsSigOutputClass,
+  icSigLinkClass               = cmsSigLinkClass,
+  icSigAbstractClass           = cmsSigAbstractClass,
+  icSigColorSpaceClass         = cmsSigColorSpaceClass,
+  icSigNamedColorClass         = cmsSigNamedColorClass,
+  icMaxEnumClass               = 0xFFFFFFFFL
+} icProfileClassSignature;
+
+typedef enum {
+    icSigXYZData               = cmsSigXYZData,
+    icSigLabData               = cmsSigLabData,
+    icSigLuvData               = cmsSigLuvData,
+    icSigYCbCrData             = cmsSigYCbCrData,
+    icSigYxyData               = cmsSigYxyData,
+    icSigRgbData               = cmsSigRgbData,
+    icSigGrayData              = cmsSigGrayData,
+    icSigHsvData               = cmsSigHsvData,
+    icSigHlsData               = cmsSigHlsData,
+    icSigCmykData              = cmsSigCmykData,
+    icSigCmyData               = cmsSigCmyData,
+    icSig2colorData            = cmsSig2colorData,
+    icSig3colorData            = cmsSig3colorData,
+    icSig4colorData            = cmsSig4colorData,
+    icSig5colorData            = cmsSig5colorData,
+    icSig6colorData            = cmsSig6colorData,
+    icSig7colorData            = cmsSig7colorData,
+    icSig8colorData            = cmsSig8colorData,
+    icSig9colorData            = cmsSig9colorData,
+    icSig10colorData           = cmsSig10colorData,
+    icSig11colorData           = cmsSig11colorData,
+    icSig12colorData           = cmsSig12colorData,
+    icSig13colorData           = cmsSig13colorData,
+    icSig14colorData           = cmsSig14colorData,
+    icSig15colorData           = cmsSig15colorData,
+    icMaxEnumData              = 0xFFFFFFFFL
+} icColorSpaceSignature;
+
+
 CMSRGBPrimaries CMSPrimaries_Rec709(.64,.33,.3,.6,.15,.06);
 CMSRGBPrimaries CMSPrimaries_Adobe(0.64, 0.33,0.21, 0.71,0.15, 0.06);
 CMSRGBPrimaries CMSPrimaries_NTSC(0.67, 0.33, 0.21, 0.71,0.14, 0.08);
@@ -46,8 +93,6 @@
 	if(!fn)
 		throw "NULL profile filename provided";
 
-	cmsErrorAction(LCMS_ERROR_SHOW);
-
 	filename=strdup(fn);
 
 	if(!(prof=cmsOpenProfileFromFile(filename,"r")))
@@ -62,14 +107,14 @@
 	if(generated)
 	{
 		Debug[TRACE] << "Saving profile to RAM for MD5 calculation." << endl;
-		size_t plen=0;
-		_cmsSaveProfileToMem(prof,NULL,&plen);
+		cmsUInt32Number plen=0;
+		cmsSaveProfileToMem(prof,NULL,&plen);
 		if(plen>0)
 		{
 			Debug[TRACE] << "Plen = " << plen << endl;
 			buflen=plen;
 			buffer=(char *)malloc(buflen);
-			if(_cmsSaveProfileToMem(prof,buffer,&plen))
+			if(cmsSaveProfileToMem(prof,buffer,&plen))
 			{
 				Debug[TRACE] << "Saved successfully" << endl;
 				md5=new MD5Digest(buffer+sizeof(icHeader),buflen-sizeof(icHeader));
@@ -117,7 +162,7 @@
 CMSProfile::CMSProfile(CMSWhitePoint &whitepoint)
 	: md5(NULL), generated(true), filename(NULL), buffer(NULL), buflen(0)
 {
-	if(!(prof=cmsCreateLabProfile(&whitepoint.whitepoint)))
+	if(!(prof=cmsCreateLab2Profile(&whitepoint.whitepoint)))
 		throw "Can't create virtual LAB profile";
 	CalcMD5();
 }
@@ -199,14 +244,14 @@
 
 bool CMSProfile::IsV4()
 {
-	Debug[TRACE] << "Profile version: " << cmsGetProfileICCversion(prof) << endl;
-	return(cmsGetProfileICCversion(prof) >= 0x04000000L);
+	Debug[TRACE] << "Profile version: " << cmsGetEncodedICCversion(prof) << endl;
+	return(cmsGetEncodedICCversion(prof) >= 0x04000000L);
 }
 
 
 enum IS_TYPE CMSProfile::GetColourSpace()
 {
-	icColorSpaceSignature sig=cmsGetColorSpace(prof);
+	cmsColorSpaceSignature sig=cmsGetColorSpace(prof);
 	switch(sig)
 	{
 		case icSigGrayData:
@@ -232,7 +277,7 @@
 {
 	if(!IsDeviceLink())
 		throw "GetDeviceLinkOutputSpace() can only be used on DeviceLink profiles!";
-	icColorSpaceSignature sig=cmsGetPCS(prof);
+	cmsColorSpaceSignature sig=cmsGetPCS(prof);
 	switch(sig)
 	{
 		case icSigGrayData:
@@ -253,14 +298,13 @@
 	}
 }
 
-
+#if 0
 const char *CMSProfile::GetName()
 {
-	const char *txt=cmsTakeProductName(prof);
-	if(txt)
-		return(txt);
-	else
-		return("unknown");
+	cmsUInt8Number profileID[17];
+        profileID[16] = '\0';
+        cmsGetHeaderProfileID(prof, profileID);
+        return((const char*)profileID);
 }
 
 
@@ -283,17 +327,20 @@
 		return("unknown");
 }
 
+#endif
 
 const char *CMSProfile::GetDescription()
 {
-	const char *txt=cmsTakeProductDesc(prof);
-	if(txt)
-		return(txt);
-	else
-		return("unknown");
-}
+	static char buf[100];
+	memset(buf, 0, 100);
+	cmsUInt32Number ret;
+	ret =  	cmsGetProfileInfoASCII(prof, cmsInfoDescription, cmsNoLanguage, cmsNoCountry, buf, 99);
 
+	if (ret) return buf;
+	return("unknown");
+}
 
+#if 0
 const char *CMSProfile::GetInfo()
 {
 	const char *txt=cmsTakeProductInfo(prof);
@@ -312,7 +359,7 @@
 	else
 		return("unknown");
 }
-
+#endif
 
 MD5Digest *CMSProfile::GetMD5()
 {
--- a/Makefile.am
+++ b/Makefile.am
@@ -7,7 +7,7 @@
 localedir=$(datadir)/locale
 
 AM_CXXFLAGS = -DLOCALEDIR=\"$(localedir)\" -Wall -I./support/ -I./miscwidgets -I./imagesource/ -I./splashscreen -I./profilemanager -I./effects \
-	$(LCMS_CFLAGS) $(GP_CFLAGS) $(GTK2_CFLAGS)
+	$(LCMS2_CFLAGS) $(GP_CFLAGS) $(GTK2_CFLAGS)
 
 bin_PROGRAMS = photoprint
 
@@ -97,7 +97,7 @@
 	stp_support/libstp_support.la \
 	pixbufthumbnail/libpixbufthumbnail.la	\
 	splashscreen/libsplashscreen.la	\
-	$(LIBINTL) $(LIBM_LIBS) $(GETOPT_LIBS) $(JPEG_LIBS) $(PNM_LIBS) $(TIFF_LIBS) $(LCMS_LIBS) $(GP_LIBS) $(GTK2_LIBS) \
+	$(LIBINTL) $(LIBM_LIBS) $(GETOPT_LIBS) $(JPEG_LIBS) $(PNM_LIBS) $(TIFF_LIBS) $(LCMS2_LIBS) $(GP_LIBS) $(GTK2_LIBS) \
 	-lX11
 
 check_PROGRAMS = menucheck carouselcheck misccheck

